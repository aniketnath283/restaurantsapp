package com.example.resturants.presentation.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.resturants.R
import com.example.resturants.databinding.ItemRestaurantBinding
import com.example.resturants.domain.entities.RestaurantDetails
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class RestaurantsAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<RestaurantsAdapter.ViewHolder>() {
    private var items: List<RestaurantDetails>? = listOf()

    fun setData(items: List<RestaurantDetails>?) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemRestaurantBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ))
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items?.get(position))
    }

    inner class ViewHolder(private val binding: ItemRestaurantBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(restaurantDetails: RestaurantDetails?) {
            binding.apply {
                textRestaurantName.text = restaurantDetails?.name
                textAddress.text = restaurantDetails?.address
                textOpenStatus.text = restaurantDetails?.openState
                val reviews = context.getString(R.string.reviews) + restaurantDetails?.ratings + " (" + restaurantDetails?.reviews + ")"
                textReviews.text = reviews
                Glide.with(context).load(restaurantDetails?.thumbnail).into(imageRestro)
            }
        }
    }
}