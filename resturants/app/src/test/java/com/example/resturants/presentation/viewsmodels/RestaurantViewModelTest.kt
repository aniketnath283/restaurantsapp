package com.example.resturants.presentation.viewsmodels

import android.location.Location
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.example.resturants.domain.entities.RestaurantDetails
import com.example.resturants.domain.entities.RestaurantInfo
import com.example.resturants.domain.location.LocationTracker
import com.example.resturants.domain.usecases.LaunchParams
import com.example.resturants.domain.usecases.RestaurantUseCase
import com.example.resturants.domain.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class RestaurantViewModelTest {

    @Mock
    lateinit var restaurantUseCase: RestaurantUseCase

    @Mock
    lateinit var locationTracker: LocationTracker

    private val testDispatcher = StandardTestDispatcher()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        runTest {
            Mockito.`when`(locationTracker.getCurrentLocation()).thenReturn(Location("").apply {
                latitude = 22.7661
                longitude = 88.3516
            })
        }
    }

    @Test
    fun test_getRestaurants() = runTest {
        Mockito.`when`(restaurantUseCase.process(LaunchParams(lat = 22.7661, long = 88.3516))).thenReturn(Resource.Success(
            RestaurantInfo(restaurantDetails = emptyList())
        ))
        val sut = RestaurantViewModel(restaurantUseCase, locationTracker)
        sut.getRestaurantInfo()
        val result = sut.restaurantInfo.first()?.data?.restaurantDetails?.size

        assertEquals(0, result)
    }

    @Test
    fun get_restaurants_with_data() = runTest {
        Mockito.`when`(restaurantUseCase.process(LaunchParams(lat = 22.7661, long = 88.3516))).thenReturn(Resource.Success(
            RestaurantInfo(restaurantDetails = listOf(
                RestaurantDetails(name = "ABCD",
                    ratings = 3.8, reviews = 200, address = "Barrackpore", openState = "Open"),
                RestaurantDetails(name = "EFGH",
                    ratings = 4.1, reviews = 150, address = "Barrackpore", openState = "Close")
            ))
        ))
        val sut = RestaurantViewModel(restaurantUseCase, locationTracker)
        sut.getRestaurantInfo()
        sut.restaurantInfo.test {
            val result = this.awaitItem()?.data?.restaurantDetails?.get(0)?.name
            assertEquals("ABCD", result)
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}