package com.example.resturants.common.base.views

import android.Manifest
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.ViewDataBinding
import com.example.resturants.common.base.viewmodels.BaseViewModel
import com.example.resturants.presentation.utils.showSnackBar
import kotlin.reflect.KClass

abstract class BaseSessionViewModelActivity<VB : ViewDataBinding, VM : BaseViewModel>(
    inflater: InflateActivityLayout<VB>?,
    viewModelClass: KClass<VM>
) : BaseViewModelActivity<VB, VM>(inflater, viewModelClass) {

    private lateinit var permissionLauncher: ActivityResultLauncher<Array<String>>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupInitialApiCalls(getViewModel())
    }

    protected fun askLocationRequestPermission() {
        permissionLauncher = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) {
            getBinding()?.onLocationPermissionStatus(getViewModel())
        }
        permissionLauncher.launch(arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
        ))
    }

    protected fun showError(errorMsg: String) {
        getBinding()?.apply {
            root.showSnackBar(errorMsg) {
                takeActionOnError(getViewModel())
            }
        }
    }

    open fun VB.onLocationPermissionStatus(vm: VM) {}

    open fun VB.takeActionOnError(vm: VM) {}

}