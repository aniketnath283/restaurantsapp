package com.example.resturants.presentation.viewsmodels

import androidx.lifecycle.viewModelScope
import com.example.resturants.common.base.entities.AppData
import com.example.resturants.common.base.viewmodels.BaseViewModel
import com.example.resturants.domain.entities.RestaurantInfo
import com.example.resturants.domain.location.LocationTracker
import com.example.resturants.domain.usecases.LaunchParams
import com.example.resturants.domain.usecases.RestaurantUseCase
import com.example.resturants.domain.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RestaurantViewModel @Inject constructor(
    private val restaurantUseCase: RestaurantUseCase,
    private val locationTracker: LocationTracker
): BaseViewModel() {

    private val _restaurantInfo: MutableStateFlow<AppData<RestaurantInfo>?> = MutableStateFlow(AppData())
    val restaurantInfo: StateFlow<AppData<RestaurantInfo>?> = _restaurantInfo

    fun getRestaurantInfo() {
        viewModelScope.launch {
            if (_restaurantInfo.value?.data != null) {
                _restaurantInfo.emit(value = _restaurantInfo.value)
            } else {
                getRestaurantInfoData()
            }
        }
    }

    private suspend fun getRestaurantInfoData() {
        _restaurantInfo.value = AppData(loadingState = true)

        locationTracker.getCurrentLocation()?.let {
            fetchUseCaseWithAnyInput(restaurantUseCase,
                LaunchParams(lat = it.latitude, long = it.longitude)) { result->
                when(result) {
                    is Resource.Success -> {
                        _restaurantInfo.value = AppData(data = result.data, loadingState = false)
                    }
                    is Resource.Error -> {
                        _restaurantInfo.value = AppData(loadingState = false, errorMsg = result.message)
                    }
                }
            }
        }
    }
}