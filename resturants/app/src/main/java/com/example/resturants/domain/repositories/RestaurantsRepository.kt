package com.example.resturants.domain.repositories

import com.example.resturants.domain.entities.RestaurantInfo
import com.example.resturants.domain.utils.Resource

interface RestaurantsRepository {
    suspend fun getNearbyRestaurants(lat: Double?, long: Double?): Resource<RestaurantInfo>
}