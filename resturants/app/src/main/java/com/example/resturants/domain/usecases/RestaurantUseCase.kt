package com.example.resturants.domain.usecases

import com.example.resturants.common.base.usecases.BaseUseCaseWithAnyInput
import com.example.resturants.domain.entities.RestaurantInfo
import com.example.resturants.domain.repositories.RestaurantsRepository
import com.example.resturants.domain.utils.Resource
import javax.inject.Inject

class RestaurantUseCase @Inject constructor(private val repository: RestaurantsRepository) :
    BaseUseCaseWithAnyInput<LaunchParams, Resource<RestaurantInfo>> {

    override suspend fun process(input: LaunchParams): Resource<RestaurantInfo> {
        return repository.getNearbyRestaurants(input.lat, input.long)
    }
}

data class LaunchParams(
    var lat: Double? = null,
    var long: Double? = null
)