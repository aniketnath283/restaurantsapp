package com.example.resturants.domain.usecases

import com.example.resturants.domain.entities.RestaurantDetails
import com.example.resturants.domain.entities.RestaurantInfo
import com.example.resturants.domain.repositories.RestaurantsRepository
import com.example.resturants.domain.utils.Resource
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class RestaurantUseCaseTest {

    @Mock
    lateinit var restaurantsRepository: RestaurantsRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun test_getRestaurants() = runTest {
        Mockito.`when`(restaurantsRepository.getNearbyRestaurants(lat = 22.7661, long = 88.3516)).thenReturn(
            Resource.Success(
            RestaurantInfo(restaurantDetails = emptyList())
        ))
        val sut = RestaurantUseCase(restaurantsRepository)
        val result = sut.process(LaunchParams(lat = 22.7661, long = 88.3516))

        assertEquals(0, result.data?.restaurantDetails?.size)
    }

    @Test
    fun get_restaurants_with_data() = runTest {
        Mockito.`when`(restaurantsRepository.getNearbyRestaurants(lat = 22.7661, long = 88.3516)).thenReturn(
            Resource.Success(
                RestaurantInfo(restaurantDetails = listOf(
                    RestaurantDetails(name = "ABCD",
                        ratings = 3.8, reviews = 200, address = "Barrackpore", openState = "Open"),
                    RestaurantDetails(name = "EFGH",
                        ratings = 4.1, reviews = 150, address = "Barrackpore", openState = "Close")
                ))
            ))
        val sut = RestaurantUseCase(restaurantsRepository)
        val result = sut.process(LaunchParams(lat = 22.7661, long = 88.3516))

        assertEquals("ABCD", result.data?.restaurantDetails?.get(0)?.name)
    }

    @After
    fun tearDown() {
    }
}