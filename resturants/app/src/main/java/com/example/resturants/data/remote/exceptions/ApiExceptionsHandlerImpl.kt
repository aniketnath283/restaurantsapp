package com.example.resturants.data.remote.exceptions

import android.app.Application
import com.example.resturants.R
import com.example.resturants.domain.exceptions.ExceptionHandler
import retrofit2.HttpException
import java.net.UnknownHostException
import javax.inject.Inject

class ApiExceptionsHandlerImpl @Inject constructor(private val app: Application) : ExceptionHandler {
    override fun handleException(exception: Exception): String {
        return when(exception) {
            is UnknownHostException -> app.getString(R.string.internet_connection_failed)
            is HttpException -> app.getString(R.string.internal_server_error)
            else -> exception.message ?: app.getString(R.string.something_went_wrong)
        }
    }
}