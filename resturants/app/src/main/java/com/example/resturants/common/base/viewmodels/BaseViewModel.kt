package com.example.resturants.common.base.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.resturants.common.base.usecases.BaseUseCaseWithAnyInput
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class BaseViewModel: ViewModel() {

    protected inline fun <I, O>fetchUseCaseWithAnyInput(
        useCase: BaseUseCaseWithAnyInput<I, O>,
        input: I,
        crossinline response: (res: O) -> Unit
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            response(useCase.process(input))
        }
    }
}