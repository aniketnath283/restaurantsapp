package com.example.resturants.domain.entities

data class RestaurantDetails(
    var name: String? = null,
    var ratings: Double ?= null,
    var reviews: Long ?= null,
    var address: String ?= null,
    var thumbnail: String ?= null,
    var openState: String ?= null
)
