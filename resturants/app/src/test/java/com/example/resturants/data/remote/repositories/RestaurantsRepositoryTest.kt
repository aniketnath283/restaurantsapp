package com.example.resturants.data.remote.repositories

import com.example.resturants.data.remote.apis.RestaurantApi
import com.example.resturants.data.remote.entities.Restaurant
import com.example.resturants.domain.exceptions.ExceptionHandler
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class RestaurantsRepositoryTest {

    @Mock
    lateinit var restaurantApi: RestaurantApi

    @Mock
    lateinit var exceptionHandler: ExceptionHandler

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun test_getRestaurants() = runTest {
        Mockito.`when`(restaurantApi.getNearbyRestaurants(ll = "")).thenReturn(
            Restaurant())
        val sut = RestaurantsRepositoryImpl(restaurantApi, exceptionHandler)
        val result = sut.getNearbyRestaurants(lat = 22.7661, long = 88.3516)

        Assert.assertEquals(0, result.data?.restaurantDetails?.size)
    }

    @Test
    fun test_getRestaurants_with_data() = runTest {
        Mockito.`when`(restaurantApi.getNearbyRestaurants(ll = "")).thenReturn(
            Restaurant(

            ))
        val sut = RestaurantsRepositoryImpl(restaurantApi, exceptionHandler)
        val result = sut.getNearbyRestaurants(lat = 22.7661, long = 88.3516)

        Assert.assertEquals(0, result.data?.restaurantDetails?.size)
    }

    @After
    fun tearDown() {
    }
}