package com.example.resturants.domain.exceptions

interface ExceptionHandler {
    fun handleException(e: Exception): String
}