package com.example.resturants.data.remote.utils

object ApiConstants {
    const val BASE_URL = "https://serpapi.com"
    const val QUERY_ENGINE = "google_maps"
    const val QUERY_Q = "Restaurant"
    const val QUERY_TYPE = "search"
    const val QUERY_API_KEY = "c50454f59f39fdede5486beb3206a7a4e220810b008ddda64e54ed16e9c4a7f7"
    const val ZOOM_CONST = "15.1z"
}