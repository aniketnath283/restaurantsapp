package com.example.resturants.di

import com.example.resturants.data.remote.exceptions.ApiExceptionsHandlerImpl
import com.example.resturants.domain.exceptions.ExceptionHandler
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ExceptionHandlerBinder {

    @Binds
    @Singleton
    abstract fun bindApiExceptionHandler(exceptionsHandlerImpl: ApiExceptionsHandlerImpl): ExceptionHandler
}