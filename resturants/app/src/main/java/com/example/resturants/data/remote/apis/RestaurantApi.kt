package com.example.resturants.data.remote.apis

import com.example.resturants.data.remote.entities.Restaurant
import com.example.resturants.data.remote.utils.ApiConstants
import retrofit2.http.GET
import retrofit2.http.Query

interface RestaurantApi {

    @GET("/search.json")
    suspend fun getNearbyRestaurants(
        @Query("engine") engine: String = ApiConstants.QUERY_ENGINE,
        @Query("q") q: String = ApiConstants.QUERY_Q,
        @Query("ll") ll: String,
        @Query("type") type: String = ApiConstants.QUERY_TYPE,
        @Query("api_key") apiKey: String = ApiConstants.QUERY_API_KEY
    ): Restaurant
}