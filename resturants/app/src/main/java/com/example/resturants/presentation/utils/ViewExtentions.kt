package com.example.resturants.presentation.utils

import android.view.View
import android.view.animation.TranslateAnimation
import com.example.resturants.R
import com.google.android.material.snackbar.Snackbar

inline var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

fun View.slideUpAnimation() {
    val animate = TranslateAnimation(
        0f,
        0f,
        this.height.toFloat(),
        0f
    )

    animate.duration = 500
    animate.fillAfter = true
    this.startAnimation(animate)
}

fun View.showSnackBar(text: String, onRetryClick: () -> Unit) {
    Snackbar.make(this, text, Snackbar.LENGTH_SHORT)
        .setAction(context.getString(R.string.retry).uppercase()) {
            onRetryClick.invoke()
        }
        .setBackgroundTint(context.getColor(R.color.black))
        .setTextColor(context.getColor(R.color.white))
        .setActionTextColor(context.getColor(R.color.red))
        .show()
}