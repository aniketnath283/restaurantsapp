package com.example.resturants.data.remote.entities

import com.google.gson.annotations.SerializedName

data class Restaurant(
    @SerializedName("local_results") var localResults: List<LocalResults> = listOf(),
    @SerializedName("serpapi_pagination") var serpapiPagination: SerpapiPagination? = SerpapiPagination()
)

data class GpsCoordinates(
    @SerializedName("latitude") var latitude: Double? = null,
    @SerializedName("longitude") var longitude: Double? = null
)

data class OperatingHours(
    @SerializedName("tuesday") var tuesday: String? = null,
    @SerializedName("wednesday") var wednesday: String? = null,
    @SerializedName("thursday") var thursday: String? = null,
    @SerializedName("friday") var friday: String? = null,
    @SerializedName("saturday") var saturday: String? = null,
    @SerializedName("sunday") var sunday: String? = null,
    @SerializedName("monday") var monday: String? = null
)

data class ServiceOptions(
    @SerializedName("dine_in") var dineIn: Boolean? = null,
    @SerializedName("drive_through") var driveThrough: Boolean? = null,
    @SerializedName("delivery") var delivery: Boolean? = null
)

data class LocalResults(
    @SerializedName("position") var position: Int? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("place_id") var placeId: String? = null,
    @SerializedName("data_id") var dataId: String? = null,
    @SerializedName("data_cid") var dataCid: String? = null,
    @SerializedName("reviews_link") var reviewsLink: String? = null,
    @SerializedName("photos_link") var photosLink: String? = null,
    @SerializedName("gps_coordinates") var gpsCoordinates: GpsCoordinates? = GpsCoordinates(),
    @SerializedName("place_id_search") var placeIdSearch: String? = null,
    @SerializedName("provider_id") var providerId: String? = null,
    @SerializedName("rating") var rating: Double? = null,
    @SerializedName("reviews") var reviews: Long? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("types") var types: List<String> = listOf(),
    @SerializedName("address") var address: String? = null,
    @SerializedName("open_state") var openState: String? = null,
    @SerializedName("hours") var hours: String? = null,
    @SerializedName("operating_hours") var operatingHours: OperatingHours? = OperatingHours(),
    @SerializedName("service_options") var serviceOptions: ServiceOptions? = ServiceOptions(),
    @SerializedName("thumbnail") var thumbnail: String? = null
)

data class SerpapiPagination(
    @SerializedName("next") var next: String? = null
)
