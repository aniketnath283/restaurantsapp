package com.example.resturants.data.remote.repositories

import com.example.resturants.data.remote.apis.RestaurantApi
import com.example.resturants.data.remote.mappers.toRestaurantInfo
import com.example.resturants.data.remote.utils.ApiConstants
import com.example.resturants.domain.entities.RestaurantInfo
import com.example.resturants.domain.exceptions.ExceptionHandler
import com.example.resturants.domain.repositories.RestaurantsRepository
import com.example.resturants.domain.utils.Resource
import javax.inject.Inject

class RestaurantsRepositoryImpl @Inject constructor(private var api: RestaurantApi,
                                                    private val exceptionHandler: ExceptionHandler): RestaurantsRepository {

    override suspend fun getNearbyRestaurants(
        lat: Double?,
        long: Double?
    ): Resource<RestaurantInfo> {
        val latLang = "@$lat,$long,${ApiConstants.ZOOM_CONST}"
        return try {
            Resource.Success(api.getNearbyRestaurants(ll = latLang).toRestaurantInfo())
        } catch (e: Exception) {
            Resource.Error(exceptionHandler.handleException(e))
        }

    }

}