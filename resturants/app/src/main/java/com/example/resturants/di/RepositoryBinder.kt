package com.example.resturants.di

import com.example.resturants.data.remote.repositories.RestaurantsRepositoryImpl
import com.example.resturants.domain.repositories.RestaurantsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryBinder {

    @Binds
    @Singleton
    abstract fun bindRestaurantRepository(restaurantsRepositoryImpl: RestaurantsRepositoryImpl): RestaurantsRepository
}