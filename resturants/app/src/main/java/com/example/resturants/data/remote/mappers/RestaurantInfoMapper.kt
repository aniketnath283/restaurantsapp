package com.example.resturants.data.remote.mappers

import com.example.resturants.data.remote.entities.Restaurant
import com.example.resturants.domain.entities.RestaurantDetails
import com.example.resturants.domain.entities.RestaurantInfo

fun Restaurant.toRestaurantDetailsData(): List<RestaurantDetails> {
    val restaurantsDetails: MutableList<RestaurantDetails> = mutableListOf()
    localResults.forEach {
        restaurantsDetails.add(RestaurantDetails().apply {
            name = it.title
            address = it.address
            thumbnail = it.thumbnail
            ratings = it.rating
            reviews = it.reviews
            openState = it.openState
        })
    }
    return restaurantsDetails
}

fun Restaurant.toRestaurantInfo(): RestaurantInfo {
    val restaurantDetails = toRestaurantDetailsData()
    return RestaurantInfo(restaurantDetails)
}