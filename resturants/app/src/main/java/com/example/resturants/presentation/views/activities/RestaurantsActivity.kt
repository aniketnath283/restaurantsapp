package com.example.resturants.presentation.views.activities

import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.resturants.common.base.views.BaseSessionViewModelActivity
import com.example.resturants.common.utils.checkLocationPermission
import com.example.resturants.databinding.ActivityRestaurantsBinding
import com.example.resturants.presentation.utils.isVisible
import com.example.resturants.presentation.utils.slideUpAnimation
import com.example.resturants.presentation.viewsmodels.RestaurantViewModel
import com.example.resturants.presentation.views.adapters.RestaurantsAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RestaurantsActivity : BaseSessionViewModelActivity<ActivityRestaurantsBinding, RestaurantViewModel>(
    ActivityRestaurantsBinding::inflate, RestaurantViewModel::class
) {
    @Inject
    lateinit var adapter: RestaurantsAdapter

    override fun ActivityRestaurantsBinding.setupViews(savedInstanceState: Bundle?) {
        this.apply {
            listRestaurants.adapter = adapter
        }
    }

    override fun setupInitialApiCalls(vm: RestaurantViewModel): (() -> Unit)? {
        if (!checkLocationPermission()) {
            askLocationRequestPermission()
        } else {
            vm.getRestaurantInfo()
        }
        return super.setupInitialApiCalls(vm)
    }

    override fun ActivityRestaurantsBinding.observeViewModel(vm: RestaurantViewModel) {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                vm.restaurantInfo.collectLatest {
                    it?.let {
                        progressBar.isVisible = it.loadingState
                        it.data?.restaurantDetails?.let {
                            adapter.setData(items = it)
                            listRestaurants.isVisible = true
                            listRestaurants.slideUpAnimation()
                        }
                        it.errorMsg?.let {
                            showError(it)
                        }
                    }
                }
            }
        }
    }

    override fun ActivityRestaurantsBinding.takeActionOnError(vm: RestaurantViewModel) {
        vm.getRestaurantInfo()
    }

    override fun ActivityRestaurantsBinding.onLocationPermissionStatus(vm: RestaurantViewModel) {
        vm.getRestaurantInfo()
    }

}