package com.example.resturants.domain.entities

import com.example.resturants.common.base.entities.AppData

data class RestaurantInfo(var restaurantDetails: List<RestaurantDetails> ?= null)
