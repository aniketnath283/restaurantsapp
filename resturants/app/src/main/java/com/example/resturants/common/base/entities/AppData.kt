package com.example.resturants.common.base.entities

open class AppData<T>(
    var data: T ?= null,
    var loadingState: Boolean = false,
    var errorMsg: String? = null
)